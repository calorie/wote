# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Wote::Application.config.secret_key_base = ENV['SECRET_KEY_BASE'] || '7ba4bf0c8c2d46748f903373fa07711ca356cdb7352571bc4ec7e0b5606f9d3803cc493fb043d9000b016241faa4cdcfb8f64eab85db5efff9a8cba3a7709b28'
