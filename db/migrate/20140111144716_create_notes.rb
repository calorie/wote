class CreateNotes < ActiveRecord::Migration
  def change
    create_table :notes do |t|
      t.string :title, :null => false, :default => 'no title'
      t.text :desc

      t.timestamps
    end
  end
end
