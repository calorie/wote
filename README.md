wote
============
experimentation & study

[link](http://wote.herokuapp.com)

```
$ bundle install --path vendor/bundle --binstubs .bundle/bin
$ rake db:setup
```

## heroku

```
$ heroku run rake db:migrate
$ heroku run rake db:seed
$ heroku open
```

reset db

```
$ heroku pg:reset DATABASE_URL
```

